const books = [
    {id : 1, title: 'node js 1', description: 'testing json expres 1'},
    {id : 2, title: 'node js 2', description: 'testing json expres 2'},
    {id : 3, title: 'node js 3', description: 'testing json expres 3'},
    {id : 4, title: 'node js 4', description: 'testing json expres 4'},
]

const findAllBooks = (req, res) => {
    //get data dari database
    const data = books
    
    //return respon json
    const result = {
        status  : '200',
        data    : data,
        message : 'succes get data'
    }

    res.json(result)
}

const getBookById = (req, res) =>{
    //get param
    const { id } = req.params
    let book
    //loop data books

    for(let i = 0; i < books.length; i++){
        if(books[i].id === Number(id)){
            book = books[i]
        }
    }

    if(book == undefined){
        // const result = {
        //     status  : '404',
        //     message : 'data not found'
        // }
        return res.status(404).json({status: `failed`, message: `data ${id} not found`})
    }else{
        const result = {
            status  : '200',
            data    : book,
            message : 'succes get data'
        }
        res.json(result)
    }
}

const saveBooks = (req, res) => {
    // mendapatkan request body
    const { title, description } = req.body

    //mendapatkan new id

    const lastItemBookId = books[books.length - 1].id
    const newIdBook = lastItemBookId + 1

    //menambahkan new book data
    const newBookData = { id: newIdBook, title: title, description: description }
    books.push(newBookData)

    //mengembalilkan respose ke client
    res.status(201).json({ status: 'oke', message: 'new book data', data: newBookData})
}

module.exports = {findAllBooks, saveBooks, getBookById}