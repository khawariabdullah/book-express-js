const { findAllBooks, getBookById, saveBooks} = require('../controller/booksController')

const router = require('express').Router()



router.get('/books', findAllBooks)
router.post('/saveBooks', saveBooks)
router.get('/books/:id', getBookById)

module.exports = router