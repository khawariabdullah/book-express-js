const express = require('express')
const app = express()
const port = 3333
const router = require('./routes/index')

app.use(express.json())
app.use(router)

app.get('/', (req, res) => {
    // res.send("hello ini express loh ya")
    res.json({
        message : "respon json"
    })
})


app.listen(port, () => console.log(`server running di port ${port}`))
